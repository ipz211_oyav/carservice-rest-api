const mysqldump = require('mysqldump');
const path = require("path");
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const OrdersQuery = require("../queries/Orders");
const WorkersQuery = require("../queries/Workers");
const RolesQuerry = require("../queries/Roles");
router.use(express.json());

router.use(async (req,res,next)=>{
    const token = req.header("authorization").replace("Bearer ", "");
    let decoded;
    try{
        decoded = await jwt.verify(token, "secretToken");
    }        
    catch(e){
        res.status(403).send("Forbidden access");
    }

    const role = await WorkersQuery.getRoleByLogin(decoded.login);
    console.log(role);
    if(role != "Адміністратор")
    {
        res.status(403).send("Forbidden access");
    }
    else
    {
        res.locals.login = decoded.login;
        next();
    }
})

router.post("/addWorker", async (req, res)=>{
    try{
        const resp = await WorkersQuery.add(req.body);
        res.status(200).json(resp);
    } catch(e)
    {
        res.status(500).json({ error: e.message })
    }
}),

router.post("/markAsDoneOrder", async(req, res)=>{
    try{
        res.status(200).send(await OrdersQuery.markAsDone(req.body.login, req.body.id));
    } catch(e)
    {
        res.status(500).send({error: e.message});
    }
})

router.get("/getMechanicOrders", async(req,res)=>{
    try{
        res.status(200).send(await OrdersQuery.mechanicOrders(req.body.login))
    } catch(e)
    {
        res.status(500).send({error: e.message});
    }
})

router.delete("/deleteWorker", async(req,res)=>{
    try{
        const resp = await WorkersQuery.remove(req.body);
        res.status(200).json(resp);
    }catch(e)
    {
        res.status(500).json({error: e.message});
    }
})

router.get("/findWorkers", async(req,res)=>{
    try{
        res.status(200).send(await WorkersQuery.findWorker(req.body.param));
    } catch(e){
        res.status(500).send({error: e.message});
    }
})


router.get("/backup", async(req, res)=>{
    const date = Date.now();
    const filePath = path.join(__dirname, `${date}backup.sql`).replace("routes", "backupDB");
    console.log(filePath);
   await mysqldump({
        connection:{
            user: "root",
            password: "root",
            database: "courseworkdb",
            host: "localhost"  
        },
        dumpToFile: filePath
    }) 
    res.status(200).sendFile(filePath);
})

router.post("/addRole", async (req,res)=>{
    try{
        res.status(200).send(await RolesQuerry.add(req.body.name));
    } catch(e)
    {
        res.status(500).send({error: e.message});
    }
})

router.patch("/editRole", async(req,res)=>{
    try{
        res.status(200).send(await RolesQuerry.edit(req.body.old, req.body.new));
    } catch(e)
    {
        res.status(500).send({error: e});
    }
})

router.delete("/deleteRole", async(req,res)=>{
    try{
        res.status(200).send(await RolesQuerry.delete(req.body.name));
    } catch(e)
    {
        res.status(500).send({error: e})
    }
})
router.get("/selectAllRoles", async(req,res)=>{
    try{
        res.status(200).send(await RolesQuerry.select());
    } catch(e)
    {
        res.status(500).send({error: e});
    }
})
module.exports = router;