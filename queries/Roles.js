const { QueryTypes} = require('sequelize');
const { sequelize } = require("../models");

module.exports = {
    table: "roles",
    add: async function(name)
    {
        try{
           return await sequelize.query(`INSERT INTO ${this.table} (role) VALUES (:name)`, {replacements:{ name:name}, type: QueryTypes.INSERT});
        } catch(e)
        {
            throw new Error(e.name);
        }
    },
    edit: async function(oldName, newName)
    {        
        return await sequelize.query(`UPDATE ${this.table} SET role = :new WHERE role = :old`,{
            replacements:{
                new: newName,
                old: oldName
            },
            type: QueryTypes.UPDATE
        })

    },
    delete: async function(name){
        return await sequelize.query(`DELETE FROM ${this.table} WHERE role = :name`,{
            replacements:{
                name: name
            }, 
            type: QueryTypes.DELETE
        });
    },

    select: async ()=>{
        return await sequelize.query("SELECT * FROM roles", {type: QueryTypes.SELECT}); 
    }
}